module Standup
  class ConfigParser

    attr_reader :to_date, :from_date, :config

    def initialize(config)
      @config = config

      help_needed?
      verbose?

      validate_config!

      @to_date, @from_date = parse_dates
    end

    private

    def validate_config!
      fail "Please provide a Gitlab access token in your configuration file 'gitlab_access_token missing'" unless config['gitlab_access_token']
    end

    def help_needed?
      return unless @config[:help]

      puts @config.command_line_help
      exit 0
    end

    def verbose?
      return unless @config[:verbose]

      puts ' ## Here is a display of the config sources and contents'
      puts @config.detailed_layers_info
      puts ' ## This the resulting merged config'
      puts @config[].to_yaml
    end

    def parse_dates
      if @config[:from]
        from_date = Time.parse(@config[:from])
      else
        from_date = (Time.now - 1.day).beginning_of_day
      end

      if @config[:to]
        to_date = Time.parse(@config[:to])
      else
        to_date = Time.now.beginning_of_day
      end

      [to_date, from_date]
    end
  end
end
