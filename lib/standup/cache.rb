require 'vcr'

module Standup
  Cache = VCR
end

Standup::Cache.configure do |c|
  c.cassette_library_dir = "#{File.expand_path('~')}/.#{Standup::BASE_NAME}/cache"
  c.default_cassette_options = {
    re_record_interval: 1.day,
    record: :new_episodes
  }
  c.ignore_request do |request|
    request.method != :get
  end
end
