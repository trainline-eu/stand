require 'gitlab'
require 'terminal-table'

module Standup
  class ReportGenerator
    def initialize(settings)
      @settings = settings
      @result = {
        'done'     => [],
        'reviewed' => [],
      }

      configure_gitlab_client
    end

    def gitlab_report!
      # Get authenticated user
      config[:username] = Gitlab.user.username

      projects_query = {
        per_page: 20,
        membership: true,
        search: config['gitlab_project_search_pattern'],
        order_by: :last_activity_at,
        sort: :desc
      }

      created_mrs_query = {
        per_page: 100,
        state: :merged,
        order_by: :updated_at,
        sort: :desc,
        scope: 'created-by-me'
      }
      assigned_mrs_query = created_mrs_query.dup
      assigned_mrs_query[:scope] = 'assigned-to-me'

      assigned_issues_query = {
        per_page: 100,
        state: :closed,
        order_by: :updated_at,
        sort: :desc,
        scope: 'assigned-to-me'
      }

      projects = Gitlab.projects(projects_query)

      projects.each do |project|
        merged_mrs = []
        closed_issues = []

        if config[:verbose]
          puts "Scanning project #{project.namespace.name}/#{project.name} (#{project.id})"
        end

        begin
          merged_mrs = Gitlab.merge_requests(project.id, created_mrs_query).auto_paginate
          Gitlab.merge_requests(project.id, assigned_mrs_query).auto_paginate do |mr|
            merged_mrs << mr
          end

          closed_issues = Gitlab.issues(project.id, assigned_issues_query).auto_paginate
        rescue Gitlab::Error::Forbidden => e
          if @settings.config[:verbose]
            puts "Error while fetching issues or merge requests from project #{project.name} (#{project.id}): #{e.inspect}"
            puts "You probably don't have access to the project."
          end
        end

        merged_mrs.each do |mr|
          validate_result!(project, mr, 'MR')
        end

        closed_issues.each do |issue|
          validate_result!(project, issue, 'ISSUE')
        end
      end

      puts Terminal::Table.new :title => 'DONE', :headings => [ 'PROJECT', 'TYPE', 'ID', 'TITLE' ], :rows => @result['done']
      puts "\n"
      puts Terminal::Table.new :title => 'REVIEWED', :headings => [ 'PROJECT', 'TYPE', 'ID', 'TITLE' ], :rows => @result['reviewed']
    end

    private

    def config
      @settings.config
    end

    def to_date
      @settings.to_date
    end

    def from_date
      @settings.from_date
    end

    def validate_result!(project, event, type='?')
      updated_at = Time.parse(event.updated_at)
      author     = event.author.username
      assignee   = event.assignee && event.assignee.username

      if updated_at < to_date && updated_at > from_date
        if author == config[:username]
          @result['done'] << [project.name, type, "##{event.iid}", event.title]
        elsif assignee == config[:username]
          @result['reviewed'] << [project.name, type, "##{event.iid}", event.title]
        end
      end
    end

    def configure_gitlab_client
      api_endpoint = config['gitlab_endpoint'] || 'https://gitlab.com/api/v4'.freeze

      Gitlab.configure do |c|
        c.endpoint = api_endpoint
        c.private_token = config['gitlab_access_token']
      end
    end
  end
end
