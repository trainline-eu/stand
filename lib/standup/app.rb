require 'easy_app_helper'

require 'standup/config_parser'
require 'standup/report_generator'

module Standup
  class App
    include EasyAppHelper

    NAME = 'Auto Standup'
    DESCRIPTION = 'Fetchs data from gitlab to report what you have done'

    def initialize
      config.config_file_base_name = Standup::BASE_NAME
      config.describes_application app_name: NAME,
                                   app_version: Standup::VERSION,
                                   app_description: DESCRIPTION
      add_script_options
    end

    def add_script_options
      config.add_command_line_section('Application Options') do |s|
        s.on :from,     'From specific day', argument: true
        s.on :to,       'To specific day',   argument: true
        s.on :"no-cache", "Don't use cache for API requests", argument: false
      end
    end

    def run
      settings = ConfigParser.new(config)

      record_interval = (settings.to_date - settings.from_date) / 10
      record_interval = 0 if config[:"no-cache"]

      Standup::Cache.use_cassette('gitlab', re_record_interval: record_interval) do
        ReportGenerator.new(settings).gitlab_report!
      end
    end
  end
end
