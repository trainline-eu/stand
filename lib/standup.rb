$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'active_support/core_ext/time'
require 'active_support/core_ext/numeric/time'

module Standup
  BASE_NAME = 'standup'.freeze
end

require 'standup/cache'
require 'standup/version'
require 'standup/app'
