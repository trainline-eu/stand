# Standup

[![Gem Version](https://badge.fury.io/rb/stand.svg)](https://badge.fury.io/rb/stand)

This gem is a simple binary to help you generate reports of your work.

## Installation

- Install with `gem install stand`
- Generate a new token on https://gitlab.example.net/profile/personal_access_tokens with the "api" scope
- Create the file `$HOME/.standup/standup.yml` with the following content:

```yaml
gitlab_access_token: 'AbCdEfGhIjKlMnOp'
gitlab_endpoint: 'https://gitlab.example.net/api/v4'
```

## Usage

And you are good to go via

```bash
~$ standup
+------------+-------+-----+------------------------------------------------+
|                                     DONE                                  |
+------------+-------+-----+------------------------------------------------+
| PROJECT    | TYPE  | ID  | TITLE                                          |
+------------+-------+-----+------------------------------------------------+
| myproject  | MR    | #1  | I broke everything                             |
| myproject  | MR    | #2  | This should fix it                             |
| myproject  | MR    | #3  | Getting there                                  |
| myproject  | ISSUE | #1  | Super feature                                  |
| myproject  | ISSUE | #2  | Great feature                                  |
+------------+-------+-----+------------------------------------------------+

+------------+-------+-----+------------------------------------------------+
|                                   REVIEWED                                |
+------------+-------+-----+------------------------------------------------+
| PROJECT    | TYPE  | ID  | TITLE                                          |
+------------+-------+-----+------------------------------------------------+
| myproject  | MR    | #4  | Not there yet                                  |
| myproject  | MR    | #5  | All set !                                      |
+------------+-------+-----+------------------------------------------------+
```

Forgot a standup two days ago? Just

```bash
~$ standup --from 2017-02-01 --to 2017-02-02
```

## Integrations

For now, Gitlab's API only.

## License

MIT Licensed © Capitaine Train SAS
